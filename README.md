
## 插件说明

UniAPP车牌实时离线扫描识别(Android平台)

标签：车牌实时识别 车牌离线识别 车牌实时扫描 车牌离线扫描 车牌实时离线识别 车牌实时离线扫描

特点：

- 1、使用方便，引入即可；
- 2、响应快速，原生体验；
- 3、完全离线，无需联网；
- 4、插件包体积小，不占用云打包资源(参考后边压缩体积教程)；
- 5、完全独立，不依赖任何第三方SDK（目前发现很多依赖百度等第三方SDK，需要单独进行购买第三方服务或者授权，此插件承诺终身使用）；
- 6、返回结果内容丰富，支持：`车牌号`、`车牌颜色`、`车牌抓拍图片`；
- 7、支持车牌类型齐全；
- 8、支持离线打包；
- 9、可进行定制；

Demo体验：

https://gitee.com/i_wangqian/uniapp-lpr/releases/download/demo/wang-PlateModule-demo.apk

效果：

<!--
![输入图片说明](img/demo.png)
![输入图片说明](img/%E5%AE%9A%E5%88%B6%E7%95%8C%E9%9D%A2.jpg)
![输入图片说明](img/%E6%99%AE%E9%80%9A%E8%AF%86%E5%88%AB.png)
![输入图片说明](img/%E5%8A%A8%E6%80%81%E6%89%AB%E6%8F%8F%E7%95%8C%E9%9D%A2.jpg)
-->

<img alt="" class="has" height="" src="img/demo.png" width="40%">
<img alt="" class="has" height="" src="img/%E5%AE%9A%E5%88%B6%E7%95%8C%E9%9D%A2.jpg" width="40%">
<img alt="" class="has" height="" src="img/%E6%99%AE%E9%80%9A%E8%AF%86%E5%88%AB.png" width="40%">
<img alt="" class="has" height="" src="img/%E5%8A%A8%E6%80%81%E6%89%AB%E6%8F%8F%E7%95%8C%E9%9D%A2.jpg" width="40%">

支持车牌：

|  序号   |  车牌类型  | 是否支持 |
| :----: | :----: | :----: |
| 1 | 单行蓝牌 | √ |
| 2 | 单行黄牌 | √ |
| 3 | 新能源车牌 | √ |
| 4 | 白色警用车牌 | √ |
| 5 | 教练车牌 | √ |
| 6 | 武警车牌 | √ |
| 7 | 双层黄牌 | √ |
| 8 | 双层武警 | √ |
| 9 | 使馆车牌 | √ |
| 10 | 港澳牌车 | √ |
| 11 | 双层农用车牌 | √ |
| 12 | 民航车牌 | √ |
| 13 | 摩托车牌 | √ |
| 14 | 危险品车牌 | √ |

## 平台兼容性

|  平台   | 是否支持 |  支持版本   |  支持CPU类型  |
| :----: | :----: | :----: | :----: |
| Android  | √ | 5.0 - 13.0 + | armeabi-v7a、arm64-v8a、x86
| iOS  | × | | |

## 原生插件通用使用流程：

- 1、购买插件，选择该插件绑定的项目；
- 2、在HBuilderX里找到项目，在manifest的app原生插件配置中勾选模块，如需要填写参数则参考插件作者的文档添加；
- 3、根据插件作者的提供的文档开发代码，在代码中引用插件，调用插件功能；
- 4、打包自定义基座，选择插件，得到自定义基座，然后运行时选择自定义基座，进行log输出测试；
- 5、开发完毕后正式云打包；

目前插件市场所有付费原生插件均不支持离线打包，此插件支持离线打包！！！

- Android 离线打包原生插件另见文档 https://nativesupport.dcloud.net.cn/NativePlugin/offline_package/android
- iOS 离线打包原生插件另见文档 https://nativesupport.dcloud.net.cn/NativePlugin/offline_package/ios

使用HBuilderX2.7.14以下版本，如果同一插件且同一appid下购买并绑定了多个包名，提交云打包界面提示包名绑定不一致时，需要在HBuilderX项目中manifest.json->“App原生插件配置”->”云端插件“列表中删除该插件重新选择；

## 使用说明

参考官网原生插件使用：
https://nativesupport.dcloud.net.cn/NativePlugin/use/use.html

uniapp插件库：
https://ext.dcloud.net.cn/plugin?id=10570

### 使用步骤


- 1、解压插件：

    解压插件zip包到uniapp项目目录nativePlugins中；

- 2、引用插件：

    1) 点击manifest.json 进行配置；
    2) 选择指定的本地插件（自动识别）：

- 3、使用插件：

``` javascript
// 一行代码引用
var plateModule = uni.requireNativePlugin("PlateModule")

// 调用识别程序
let plateOptions = {
					'interval': this.interval, // 识别间隔时间（单位：毫秒，默认为200）
					'showText': this.showText, // 是否显示提示文字
					'openFlash': this.openFlash, // 是否打开闪光灯
					'autoFocus': this.autoFocus, // 是否自动对焦
					'saveImage': this.saveImage, // 是否保存并返回图片
					'accuracy': (this.accuracy/100).toFixed(2), // 识别准确率(取值范围：0.0-1.0。准确率大于该值才返回结果，值越大识别越准确但也会越慢，需要测试出来最适合自己的准确率)
					'quality': this.quality // 图像压缩率(取值范围：0-100。 0:压缩100%，100:不压缩)
				}
				
				console.log('plateOptions ->', plateOptions)

				//带参数
				plateModule.startPlate(
					plateOptions,
					(ret) => {
						console.log("showLPR result : ", ret);
						modal.toast({
							message: ret,
							duration: 1.5
						});
						//解析
						this.success = true;
						if (ret.number) {
							this.number = ret.number;
						}
						if (ret.color) {
							this.color = ret.color;
						}
						if (ret.image) {
							this.image = ret.image;
							this.imagePath = "file:///" + ret.image;
						}

					}
				);

```

- 3、压缩体积教程：

将插件目录中文件`lib-lpr-release.aar`使用压缩工具打开，根据自己使用的Android平台进行保留`jni`目录下`.so`文件，即删除掉多余的平台目录；

目录结构：

```
├─ package.json
├─ android
│  ├── res
│  │── libs
│  │── assets
│  │── uniplugin_lpr-release.aar
│  └── lib-lpr-release.aar
└─ ios
```

`lib-lpr-release.aar`文件压缩包中目录结构：

```
├─ AndroidManifest.xml
├─ classes.jar
├─ res
├─ assets
├─ jni
│  │── armeabi-v7a
│  │── arm64-v8a
│  └── x86
└─ios
```

举例：如果当前平台为`armeabi-v7a`,则只需要保留`armeabi-v7a`目录即可，其他同级目录`arm64-v8a`、`x86`均可删除；

### 完整代码示例

``` javascript
<template>
	<view class="button-sp-area">
		<view class="uni-title">设置参数：</view>
		<view style="text-align: center;">
			<checkbox-group @change="changeOptions">
				<view style="text-align: center;">
					<checkbox value="showText" :checked="showText">显示文字</checkbox>	
					<checkbox value="openFlash" :checked="openFlash">打开闪光</checkbox>
				</view>
				<view style="text-align: center;">
					<checkbox value="autoFocus" :checked="autoFocus">自动对焦</checkbox>
					<checkbox value="saveImage" :checked="saveImage">返回图片</checkbox>
				</view>
			</checkbox-group>
		</view>
		<view class="uni-title">设置准确率（建议75-80）：</view>
		<view style="text-align: center;">
			<slider min="1" max="100" step="1" :value="accuracy" show-value @change="changeSlider" />
		</view>
		<button type="primary" plain="true" @click="showLPR()">点击识别车牌</button>
		<button type="primary" plain="true" @click="openCamera()">点击拍照</button>
		<view style="margin-top: 20px; padding: 20px;" v-show="this.success == true">
			<view style="text-align: center;color: lightslategray;">
				<text>车牌号：{{number}}</text>
			</view>
			<view style="text-align: center;color: lightslategray;">
				<text>车牌颜色：{{color}}</text>
			</view>
			<view style="text-align: center;color: lightslategray;">
				<text>图片地址：{{image}}</text>
			</view>
			<view style="text-align: center;color: lightslategray;">
				<image :src="imagePath"></image>
			</view>
		</view>
	</view>
</template>

<script>
	const modal = uni.requireNativePlugin('modal');
	// 获取 module
	var plateModule = uni.requireNativePlugin("wang-PlateModule")
	console.log('plateModule ->', plateModule)
	
	export default {
		data() {
			return {
				items: [
					{
						value: 'default',
						name: '默认',
						checked: 'true'
					},
					{
						value: 'scan',
						name: '经典动态扫描'
					},
				],
				success: false,
				number: "",
				color: "",
				image: "",
				imagePath: "",
				// 车牌识别默认参数
				interval: 500, // 识别间隔时间（单位：毫秒，默认为200）
				showText: true, // 是否显示提示文字
				autoFocus: true, // 是否自动对焦
				openFlash: false, // 是否打开闪光灯
				saveImage: true, // 是否保存并返回图片
				accuracy: 70, // 识别准确率(取值范围：0.0-1.0。准确率大于该值才返回结果，值越大识别越准确但也会越慢，需要测试出来最适合自己的准确率)
				quality: 80 // 图像压缩率(取值范围：0-100。 0:压缩100%，100:不压缩)
			}
		},
		methods: {
			changeActivity: function(e){
				var value = e.detail.value
				console.log('value ->', value)
				this.activity = value
			},
			changeOptions: function(e){
				var values = e.detail.value
				console.log('values ->', values)
				this.showText = values.indexOf('showText') > -1
				this.autoFocus = values.indexOf('autoFocus') > -1
				this.openFlash = values.indexOf('openFlash') > -1
				this.saveImage = values.indexOf('saveImage') > -1
			},
			changeSlider: function(e){
				var value = e.detail.value
				console.log('value ->', value)
				this.accuracy = value
			},
			openCamera() {
				console.log('openCamera...')
				uni.chooseImage({
					count: 1, //默认9
					sizeType: ['original', 'compressed'], //可以指定是原图还是压缩图，默认二者都有
					sourceType: ['camera'], //album 从相册选图，camera 使用相机，默认二者都有。如需直接开相机或直接选相册，请只使用一个选项
					success: function (res) {
						console.log(JSON.stringify(res.tempFilePaths));
					}
				});
			},
			showLPR() {
				console.log('showLPR...')
				
				let plateOptions = {
					'interval': this.interval, // 识别间隔时间（单位：毫秒，默认为200）
					'showText': this.showText, // 是否显示提示文字
					'openFlash': this.openFlash, // 是否打开闪光灯
					'autoFocus': this.autoFocus, // 是否自动对焦
					'saveImage': this.saveImage, // 是否保存并返回图片
					'accuracy': (this.accuracy/100).toFixed(2), // 识别准确率(取值范围：0.0-1.0。准确率大于该值才返回结果，值越大识别越准确但也会越慢，需要测试出来最适合自己的准确率)
					'quality': this.quality // 图像压缩率(取值范围：0-100。 0:压缩100%，100:不压缩)
				}
				
				console.log('plateOptions ->', plateOptions)

				//带参数
				plateModule.startPlate(
					plateOptions,
					(ret) => {
						console.log("showLPR result : ", ret);
						modal.toast({
							message: ret,
							duration: 1.5
						});
						//解析
						this.success = true;
						if (ret.number) {
							this.number = ret.number;
						}
						if (ret.color) {
							this.color = ret.color;
						}
						if (ret.image) {
							this.image = ret.image;
							this.imagePath = "file:///" + ret.image;
						}

					}
				);
			}
		}
	}
</script>

<style>

</style>


```
